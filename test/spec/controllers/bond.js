'use strict';

describe('Controller: BondCtrl', function () {

  // load the controller's module
  beforeEach(module('capsotestApp'));

  var BondCtrl,
    CompaniesSrv,
    NgTableParams = jasmine.createSpy(NgTableParams),
    $q,
    scope,
    bond = {
      "prices": [
        {
          "timestamp": "2016-09-30T09:59:59.181Z",
          "price": 39.73998429020867
        },
        {
          "timestamp": "2016-09-30T11:59:59.181Z",
          "price": 40.278512756340206
        },
        {
          "timestamp": "2016-09-30T13:59:59.181Z",
          "price": 40.74003898492083
        },
        {
          "timestamp": "2016-09-30T15:59:59.181Z",
          "price": 41.320825059432536
        },
        {
          "timestamp": "2016-09-30T17:59:59.181Z",
          "price": 42.186832713894546
        }
      ],
      "issuerId": 1,
      "cusip": 3125412,
      "couponRate": 8.21
    };

  // Initialize the controller
  beforeEach(inject(function ($controller, $rootScope, _$q_, _CompaniesSrv_) {
    scope = $rootScope.$new();
    $q = _$q_;
    CompaniesSrv = _CompaniesSrv_;

    spyOn(CompaniesSrv, 'get').and.callFake(function () {
      return $q.when('test');
    });

    BondCtrl = $controller('BondCtrl', {
      CompaniesSrv: CompaniesSrv,
      NgTableParams: NgTableParams,
      bond: bond
    });
  }));

  it('should set the bond info in the data property', function () {
    expect(BondCtrl.data).toBe(bond);
  });

  it('should initialise NgTableParams', function () {
    expect(NgTableParams).toHaveBeenCalledWith({
        count: 10,
        sorting: {timestamp: 'desc'}
      },
      {
        data: bond.prices
      });
  });

  it('should set company info in company property', function () {
    scope.$apply();

    expect(CompaniesSrv.get).toHaveBeenCalledWith(bond.issuerId);
    expect(BondCtrl.company).toBe('test');
  });
});
