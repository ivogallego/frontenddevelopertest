'use strict';

describe('Controller: CompanyCtrl', function () {

  // load the controller's module
  beforeEach(module('capsotestApp'));

  var CompanyCtrl,
    scope,
    bonds = [
      {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "id": 1,
        "issuerId": 1,
        "cusip": 3125412,
        "couponRate": 8.21
      },
      {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "id": 2,
        "issuerId": 1,
        "cusip": 22222,
        "couponRate": 2.22
      }],
    company = {
      companyId: 1,
      legalName: "Coca Cola Inc.",
      address: "New York",
      revenue: 1000000000,
      employees: 23450,
      industry: "Food"
    };

  // Initialize the controller
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CompanyCtrl = $controller('CompanyCtrl', {
      company: company,
      bonds: bonds
    });
  }));

  it('should set company details in data propery', function () {
    expect(CompanyCtrl.data).toBe(company);
  });

  it('should set lastPrice property in all bonds', function () {
    expect(_.all(CompanyCtrl.bondsSummary, 'lastPrice')).toBeTruthy();
  });
});
