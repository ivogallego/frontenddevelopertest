'use strict';

describe('Controller: CompaniesCtrl', function () {

  // load the controller's module
  beforeEach(module('capsotestApp'));

  var CompaniesCtrl,
    scope,
    $window,
    bonds = [
      {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "id": 1,
        "issuerId": 1,
        "cusip": 3125412,
        "couponRate": 8.21
      },
      {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "id": 2,
        "issuerId": 2,
        "cusip": 22222,
        "couponRate": 2.22
      }],
    companies = {
      data: {
        results: [
          {
            companyId: 1,
            legalName: "Coca Cola Inc.",
            address: "New York",
            revenue: 1000000000,
            employees: 23450,
            industry: "Food"
          },
          {
            companyId: 2,
            legalName: "Pepsi Cola Inc.",
            address: "London",
            revenue: 400000000,
            employees: 5430,
            industry: "Food"
          }
        ],
        total: 2
      }
    };

  // Initialize the controller
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    $window = {
      Date: function () {
        return moment('2016-10-01').toDate();
      }
    };
    CompaniesCtrl = $controller('CompaniesCtrl', {
      $scope: scope,
      $window: $window,
      companies: companies,
      bonds: bonds
    });
  }));

  it('should set companies list in companies property', function () {
    expect(CompaniesCtrl.companies).toBe(companies.data.results);
  });

  it('should set bondsPeriod as "day" by default', function () {
    expect(CompaniesCtrl.bondsPeriod).toBe('day');
  });

  it('should set a list of bonds with variation value', function () {
    expect(angular.isArray(CompaniesCtrl.highestMoving)).toBeTruthy();
    expect(_.all(CompaniesCtrl.highestMoving), 'variation');
  });

  it('should recalculate highest price bond change when period is changed', function () {
    spyOn(CompaniesCtrl, 'changePeriod');
    scope.$apply();
    CompaniesCtrl.bondsPeriod = 'week';
    scope.$apply();

    expect(CompaniesCtrl.changePeriod).toHaveBeenCalled();
  });
});
