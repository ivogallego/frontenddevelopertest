'use strict';

describe('Service: BondsSrv', function () {

  // load the controller's module
  beforeEach(module('capsotestApp'));

  var BondsSrv,
    $httpBackend,
    bonds = {
      1: {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "issuerId": 1,
        "cusip": 3125412,
        "couponRate": 8.21
      },
      2: {
        "prices": [
          {
            "timestamp": "2016-09-30T09:59:59.181Z",
            "price": 39.73998429020867
          },
          {
            "timestamp": "2016-09-30T11:59:59.181Z",
            "price": 40.278512756340206
          },
          {
            "timestamp": "2016-09-29T13:59:59.181Z",
            "price": 40.74003898492083
          },
          {
            "timestamp": "2016-09-29T15:59:59.181Z",
            "price": 41.320825059432536
          },
          {
            "timestamp": "2016-09-29T17:59:59.181Z",
            "price": 42.186832713894546
          }
        ],
        "issuerId": 1,
        "cusip": 22222,
        "couponRate": 2.22
      }
    };

  // Initialize the controller
  beforeEach(inject(function (_$httpBackend_, _BondsSrv_) {
    $httpBackend = _$httpBackend_;
    BondsSrv = _BondsSrv_;

    $httpBackend.expectGET('https://s3-eu-west-1.amazonaws.com/capso.test.data/bondmaster.json').respond(bonds);
  }));

  it('should return the list of bonds with the id property', function () {
    BondsSrv.list().then(function (result) {
      expect(_.all(result, 'id')).toBeTruthy();
    });

    $httpBackend.flush();
  });

  it('should return a bond passing its id', function () {
    BondsSrv.get('1').then(function(result) {
      expect(result.cusip).toBe(3125412);
    });

    $httpBackend.flush();
  });

  it('should get a list of bonds passing its company id', function () {
    BondsSrv.getByCompany('1').then(function (result) {
      expect(result.length).toBe(2);
    });

    $httpBackend.flush();
  });
});
