'use strict';

describe('Service: CompaniesSrv', function () {

  // load the controller's module
  beforeEach(module('capsotestApp'));

  var CompaniesSrv,
    $httpBackend,
    companies = {
      results: [
        {
          companyId: 1,
          legalName: "Coca Cola Inc.",
          address: "New York",
          revenue: 1000000000,
          employees: 23450,
          industry: "Food"
        },
        {
          companyId: 2,
          legalName: "Pepsi Cola Inc.",
          address: "London",
          revenue: 400000000,
          employees: 5430,
          industry: "Food"
        }
      ],
      total: 2
    };

  // Initialize the controller
  beforeEach(inject(function (_$httpBackend_, _CompaniesSrv_) {
    $httpBackend = _$httpBackend_;
    CompaniesSrv = _CompaniesSrv_;

    $httpBackend.expectGET('https://s3-eu-west-1.amazonaws.com/capso.test.data/companies.json').respond(companies);
  }));

  it('should return the list of companies', function () {
    CompaniesSrv.list().then(function (result) {
      expect(result.data).toEqual(companies);
    });

    $httpBackend.flush();
  });

  it('should return a company passing its id', function () {
    CompaniesSrv.get('1').then(function(result) {
      expect(result).toEqual(companies.results[0]);
    });

    $httpBackend.flush();
  });
});
