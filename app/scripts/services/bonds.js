'use strict';

angular.module('capsotestApp')
  .service('BondsSrv', BondsSrv);

function BondsSrv($http) {
  var service = this;

  service.get = get;
  service.getByCompany = getByCompany;
  service.list = list;


  function get(bondId) {
    return service
      .list()
      .then(function (response) {
        return _.find(response, {id: bondId});
      })
  }

  function getByCompany(companyId) {
    return service
      .list()
      .then(function (response) {
        return _.filter(response, function (b) {
          return b.issuerId == companyId
        });
      });
  }

  function list() {
    return $http.get('https://s3-eu-west-1.amazonaws.com/capso.test.data/bondmaster.json', {cache: true})
      .then(function (response) {
        return _.map(response.data, function (bond, id) {
          return angular.extend({}, bond, {id: id});
        });
      });
  }
}
