'use strict';

angular.module('capsotestApp')
  .service('CompaniesSrv', CompaniesSrv);

function CompaniesSrv($http) {
  var service = this;

  service.get = get;
  service.list = list;


  function get(companyId) {
    return service
      .list()
      .then(function(response) {
        return _.find(response.data.results, function(company) {
          return company.companyId == companyId;
        });
      })
  }

  function list() {
    return $http.get('https://s3-eu-west-1.amazonaws.com/capso.test.data/companies.json', {cache: true});
  }
}
