'use strict';

/**
 * @ngdoc overview
 * @name capsotestApp
 * @description
 * # capsotestApp
 *
 * Main module of the application.
 */
angular.module('capsotestApp', [
  'ui.router',
  'ngAnimate',
  'ngResource',
  'ngSanitize',
  'ngTouch',
  'ngTable'
]).config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('main', {
      url: '/',
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .state('companies', {
      url: '/companies',
      templateUrl: 'views/companies.html',
      controller: 'CompaniesCtrl as Companies',
      resolve: {
        companies: function (CompaniesSrv) {
          return CompaniesSrv.list();
        },
        bonds: function(BondsSrv) {
          return BondsSrv.list();
        }
      }
    })
    .state('company', {
      url: '/companies/{companyId}',
      templateUrl: 'views/company.html',
      controller: 'CompanyCtrl as Company',
      resolve: {
        company: function (CompaniesSrv, $stateParams) {
          return CompaniesSrv.get($stateParams.companyId);
        },
        bonds: function(BondsSrv, $stateParams) {
          return BondsSrv.getByCompany($stateParams.companyId);
        }
      }
    })
    .state('bond', {
      url: '/bonds/{bondId}',
      templateUrl: 'views/bond.html',
      controller: 'BondCtrl as Bond',
      resolve: {
        bond: function(BondsSrv, $stateParams) {
          return BondsSrv.get($stateParams.bondId);
        }
      }
    });
});
