'use strict';

angular.module('capsotestApp')
  .controller('CompanyCtrl', CompanyCtrl);


function CompanyCtrl(company, bonds) {
  var controller = this;

  controller.data = company;
  controller.bondsSummary = _.map(bonds, function (bond) {
    bond.lastPrice = _.max(bond.prices, function(price) {
      return (new Date(price.timestamp)).getTime();
    }).price;

    return bond;
  });
}
