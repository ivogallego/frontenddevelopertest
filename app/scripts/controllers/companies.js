'use strict';

angular.module('capsotestApp')
  .controller('CompaniesCtrl', CompaniesCtrl);

/**
 * @ngdoc function
 * @name capsotestApp.controller:CompaniesCtrl
 * @description
 * # CompaniesCtrl
 * Controller for listing companies ang high moving price bonds
 */
function CompaniesCtrl($scope, $window, companies, bonds) {
  var controller = this;

  controller.companies = companies.data.results;
  controller.bondsPeriod = 'day';

  controller.changePeriod = changePeriod;

  _init();

  function _init() {
    _.each(bonds, function (bond) {
      bond.company = _.find(controller.companies, {companyId: bond.issuerId});

      bond.lastPrice = _.max(bond.prices, function (price) {
        return (new Date(price.timestamp)).getTime();
      }).price;
    });

    controller.changePeriod();

    $scope.$watch(function () {
      return controller.bondsPeriod;
    }, function (newVal, oldVal) {
      if (newVal !== oldVal) {
        controller.changePeriod();
      }
    });
  }

  function changePeriod() {
    _.each(bonds, function (bond) {
      var today = new $window.Date(),
        dateLimit = moment(today).subtract(1, controller.bondsPeriod.substr(0, 1)),
        filteredPrices = _.filter(bond.prices, function (price) {
          var priceDate = moment(price.timestamp);
          return priceDate.isBefore(moment(today)) && priceDate.isAfter(dateLimit);
        }),
        sortedPrices = _.sortByOrder(filteredPrices, ['timestamp'], ['desc']);

      bond.variation = _.first(sortedPrices).price - _.last(sortedPrices).price;
    });

    controller.highestMoving = _.take(_.sortByOrder(bonds, ['variation'], ['desc']), 5);
  }
}
