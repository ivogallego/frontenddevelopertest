'use strict';

angular.module('capsotestApp')
  .controller('BondCtrl', BondCtrl);


function BondCtrl(CompaniesSrv, NgTableParams, bond) {
  var controller = this;

  controller.data = bond;
  controller.tableParams = new NgTableParams(
    {
      count: 10,
      sorting: {timestamp: 'desc'}
    },
    {
      data: bond.prices
    });

  CompaniesSrv.get(bond.issuerId).then(function (company) {
    controller.company = company;
  });
}
